create sequence hibernate_sequence
	increment 1
	minvalue 1
	maxvalue 9223372036854775807
	start 2
	cache 1;
alter table hibernate_sequence
	owner to postgres;


create table groups(
										 group_id bigint primary key,
										 group_name varchar(50) not null,
										 last_updated timestamp with time zone not null default current_timestamp);


create table users(
										user_id bigint primary key,
										first_name varchar(50) not null,
										last_name varchar(50) not null,
										group_id bigint not null,
										login varchar(50) not null,
										password text not null,
										last_updated timestamp with time zone not null default current_timestamp,
										foreign key(group_id) references groups(group_id) on delete cascade);


create table roles(
										role_id bigint primary key,
										role_name varchar(20));

insert into roles(role_id, role_name) values (1, 'ROLE_ADMIN'), (2, 'ROLE_USER');


create table user_roles(
												 user_id bigint not null,
												 role_id bigint not null,
												 primary key(user_id, role_id),
												 foreign key (user_id) references users(user_id) on delete cascade,
												 foreign key (role_id) references roles(role_id) on delete cascade);


create table accounts(
											 account_id bigint primary key,
											 user_id bigint not null,
											 account_name varchar(50) not null,
											 account_balance float,
											 last_updated timestamp with time zone not null default current_timestamp,
											 foreign key(user_id) references users(user_id) on delete cascade);

create or replace view bank_service_userdata as
	select u.user_id, role_name, login, password, group_id, last_updated
	from users u, user_roles ur, roles r
	where u.user_id = ur.user_id and ur.role_id = r.role_id