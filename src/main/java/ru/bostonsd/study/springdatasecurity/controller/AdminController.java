package ru.bostonsd.study.springdatasecurity.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * simple controller for access restriction test
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @GetMapping
    public ResponseEntity adminData(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return new ResponseEntity<>(auth.getName(), HttpStatus.OK);
    }
}
