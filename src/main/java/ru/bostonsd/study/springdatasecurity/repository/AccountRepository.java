package ru.bostonsd.study.springdatasecurity.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.study.springdatasecurity.model.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long>  {

}
