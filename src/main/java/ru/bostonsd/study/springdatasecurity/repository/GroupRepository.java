package ru.bostonsd.study.springdatasecurity.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.study.springdatasecurity.model.Group;

@Repository
public interface GroupRepository extends PagingAndSortingRepository<Group, Long> {

}
