package ru.bostonsd.study.springdatasecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.bostonsd.study.springdatasecurity.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Bulk deletion
     * @param Ids
     */
    @Modifying
    @Query(value = "delete from users where user_id in (:ids)", nativeQuery = true)
    void deleteByIdList(@Param("ids") List<Long> Ids);


    @Modifying
    void deleteByIdIn(List<Long> Ids);

    @Query("from User where id = :id")
    User hqlFindByName(@Param("id") Long userId);

    Page<User> findAll(Pageable pageable);

    Optional<User> findByLogin(String login);
}
