package ru.bostonsd.study.springdatasecurity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.bostonsd.study.springdatasecurity.model.Role;
import ru.bostonsd.study.springdatasecurity.model.User;
import ru.bostonsd.study.springdatasecurity.service.impl.GroupServiceImpl;
import ru.bostonsd.study.springdatasecurity.service.impl.RoleServiceImpl;
import ru.bostonsd.study.springdatasecurity.service.impl.UserServiceImpl;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Slf4j
@SpringBootApplication
@EnableAutoConfiguration(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class, MongoRepositoriesAutoConfiguration.class})
public class SpringJpaApplication {


	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringJpaApplication.class, args);
		GroupServiceImpl groupService = context.getBean("groupServiceImpl", GroupServiceImpl.class);
		UserServiceImpl userService = context.getBean("userServiceImpl", UserServiceImpl.class);
		RoleServiceImpl roleService = context.getBean("roleServiceImpl", RoleServiceImpl.class);
		PasswordEncoder passwordEncoder = context.getBean("passwordEncoder", PasswordEncoder.class);


/*
		User newUser = new User();
		newUser.setFirstName("Stepa");
		newUser.setLastName("Gaechkin");
		newUser.setGroupId(44L);
		newUser.setLogin("stepan");
		newUser.setPassword(passwordEncoder.encode("somepass"));


		Role adminRole = new Role();
		adminRole.setId(1L);
		adminRole.setName("ADMIN");

		Role userRole = new Role();
		userRole.setId(2L);
		userRole.setName("USER");

		Set<Role> roles = new LinkedHashSet<>();
		//roles.add(adminRole);
		roles.add(userRole);
		newUser.setRoles(roles);

		newUser.setLastUpdated(LocalDateTime.now());
		userService.persist(newUser);
		*/


	}

}
