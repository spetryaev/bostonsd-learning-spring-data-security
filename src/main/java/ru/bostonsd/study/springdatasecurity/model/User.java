package ru.bostonsd.study.springdatasecurity.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Entity(name = "User")
@Table(name = "users")
public class User {

    @Id
    @Setter(AccessLevel.PRIVATE)
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "group_id", nullable = false)
    private Long groupId;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_roles",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    @OrderBy("role_id")
    private Set<Role> roles;

    @OneToMany(mappedBy = "userId", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @OrderBy("account_id")
    private Set<Account> accountSet;

    @Column(name = "last_updated")
    private LocalDateTime lastUpdated;

    public User(){}

    public User(User u) {
        setId(u.getId());
        setFirstName(u.getFirstName());
        setLastName(u.getLastName());
        setGroupId(u.getGroupId());
        setLogin(u.getLogin());
        setPassword(u.getPassword());
        setRoles(u.getRoles());
        setAccountSet(u.getAccountSet());
        setLastUpdated(u.getLastUpdated());
    }
}
