package ru.bostonsd.study.springdatasecurity.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @Column(name = "account_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "account_name", nullable = false)
    private String name;

    @Column(name = "account_balance", nullable = false)
    private float balance;

    @Column(name = "last_updated")
    private LocalDateTime lastUpdated;

}
