package ru.bostonsd.study.springdatasecurity.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Custom User Details
 * Spring Security processes a User Details class' data
 * So, we need to customize it
 */
public class CustomUserDetails extends User implements UserDetails {

    public CustomUserDetails(final User user){
        super(user);
    }

    /**
     * Provide Authorities (roles)
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles().stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName())).collect(Collectors.toSet());
    }

    /**
     * Provide Username (login)
     * @return
     */
    @Override
    public String getUsername() {
        return super.getLogin();
    }

    /**
     * Provide User password
     * @return
     */
    @Override
    public String getPassword() {
        return super.getPassword();
    }

    /*
    other details
     */

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
