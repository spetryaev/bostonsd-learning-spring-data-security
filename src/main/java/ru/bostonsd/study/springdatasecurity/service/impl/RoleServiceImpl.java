package ru.bostonsd.study.springdatasecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.study.springdatasecurity.model.Role;
import ru.bostonsd.study.springdatasecurity.repository.RoleRepository;
import ru.bostonsd.study.springdatasecurity.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;


    public Role getRole(Long id){
        return roleRepository.getOne(id);
    }
}
