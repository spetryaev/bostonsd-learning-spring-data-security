package ru.bostonsd.study.springdatasecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.bostonsd.study.springdatasecurity.model.User;
import ru.bostonsd.study.springdatasecurity.repository.UserRepository;
import ru.bostonsd.study.springdatasecurity.service.UserService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Transactional
    @Override
    public boolean persist(User user) {
        userRepository.save(user);
        return true;
    }

    @Override
    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    @Transactional
    public boolean bulkDeleteUsersByIds(List<Long> Ids) {
        userRepository.deleteByIdList(Ids);
        return true;
    }


    @Transactional
    public boolean notBulkDeleteUsersByIds(List<Long> Ids){
        userRepository.deleteByIdIn(Ids);
        return true;
    }

    public User findById(Long id){
        return userRepository.hqlFindByName(id);
    }


    public Page findall(Pageable pageable){
        return userRepository.findAll(pageable);
    }

    public void deleteAll(List<User> users){
        userRepository.deleteInBatch(users);
    }
}
