package ru.bostonsd.study.springdatasecurity.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.study.springdatasecurity.model.Group;
import ru.bostonsd.study.springdatasecurity.repository.GroupRepository;
import ru.bostonsd.study.springdatasecurity.service.GroupService;

import javax.transaction.Transactional;
import java.util.Optional;


@Slf4j
@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository repository;

    @Override
    public Iterable<Group> getAllGroups() {
        log.info("Obtaining all data");
        return repository.findAll();
    }

    @Override
    public boolean persistGroup(Group group) {
        repository.save(group);
        return true;
    }

    @Transactional
    public Optional<Group> find(Long id){
        return repository.findById(id);
    }
}
