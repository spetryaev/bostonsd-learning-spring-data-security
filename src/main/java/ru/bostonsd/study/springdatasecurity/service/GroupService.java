package ru.bostonsd.study.springdatasecurity.service;

import ru.bostonsd.study.springdatasecurity.model.Group;


public interface GroupService {

    Iterable<Group> getAllGroups();

    boolean persistGroup(Group group);
}
