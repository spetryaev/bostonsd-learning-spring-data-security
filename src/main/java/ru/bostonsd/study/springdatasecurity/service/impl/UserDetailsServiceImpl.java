package ru.bostonsd.study.springdatasecurity.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.bostonsd.study.springdatasecurity.model.CustomUserDetails;
import ru.bostonsd.study.springdatasecurity.model.User;
import ru.bostonsd.study.springdatasecurity.repository.UserRepository;

import java.util.Optional;

/**
 * Custom User Details Service for user data loading (from the database)
 */
@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByLogin(username);
        if (optionalUser.isPresent()){
            return new CustomUserDetails(optionalUser.get());
        } else {
            log.error("There is no user with such username: '" + username + "'.");
            throw new UsernameNotFoundException("There is no user with such login");
        }
    }
}
