package ru.bostonsd.study.springdatasecurity.service;

import ru.bostonsd.study.springdatasecurity.model.User;


public interface UserService {

    boolean persist(User user);

    Iterable<User> getAll();

}
