package ru.bostonsd.study.springdatasecurity.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AuthenticationEventListener implements ApplicationListener<AbstractAuthenticationEvent> {
    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent authenticationEvent) {
        //if (authenticationEvent instanceof InteractiveAuthenticationSuccessEvent){
            //ignore successful authentication
        //} else {
            Authentication authentication = authenticationEvent.getAuthentication();
            String auditMessage = "Login attempt with username: '" + authentication.getName() + "'\t\tSuccess: " + authentication.isAuthenticated();
            log.info(auditMessage);
        //}
    }
}
